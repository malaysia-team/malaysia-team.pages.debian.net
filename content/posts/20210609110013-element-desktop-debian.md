+++ 
draft = false
date = 2021-06-09T11:00:13+08:00
title = "Installing Element (Matrix) desktop on Debian (alternative)"
description = ""
slug = "matrix-element-desktop-debian"
authors = ['Robbi Nespu <robbinespu AT SPAMFREE gmail DOT com>']
tags = ['matrix', 'element', 'package', 'ITP', 'riot', 'irc']
categories = []
externalLink = ""
series = []
+++
## Introduction
Currently, there is no `element-desktop` (matrix chat client, formerly known as `riot`) package available on debian repository. You may check ITP bug on [https://bugs.debian.org/866502](https://bugs.debian.org/866502) for details and updates.

There is another alternative such as nheko, spectral, weechat-matrix and fews more but nothing can beat `element-desktop` for full features such as E2EE (End to end encryption), room administration, labs, session verification and more. 

I tried to install `element-desktop` package from https://packages.riot.im/debian repository and it worked very well on my Debian 11 (Bullseye).

```bash
$ apt-cache policy element-desktop
element-desktop:
  Installed: 1.7.30
  Candidate: 1.7.30
  Version table:
 *** 1.7.30 500
        500 https://packages.riot.im/debian default/main amd64 Packages
        100 /var/lib/dpkg/status
```

Well if you curious on stuff about license and source code, you may check their repository [here](https://github.com/vector-im/element-desktop/). I saw that it use [**Apache License 2.0**](https://github.com/vector-im/element-desktop/blob/develop/LICENSE).

## How to (steps)
1. Delete existing GPG key from riot-im
```bash
$ sudo rm /usr/share/keyrings/riot-im-archive-keyring.gpg
```

2. Download riot-im PGP key and give correct permission
```bash
$ sudo wget -O /usr/share/keyrings/riot-im-archive-keyring.gpg https://packages.riot.im/debian/riot-im-archive-keyring.gpg
$ sudo chmod 644 /usr/share/keyrings/riot-im-archive-keyring.gpgs
```

3. Add `riot-im.list` repository
```bash
$ echo "deb [signed-by=/usr/share/keyrings/riot-im-archive-keyring.gpg] https://packages.riot.im/debian/ default main" | sudo tee /etc/apt/sources.list.d/riot-im.list
```

4. Update repository metadata and install `element-desktop` package
```bash
$ sudo apt update; sudo apt install element-desktop
```

## Closing
Now you have fully features matrix client, register new account or login your existing account to use it. Don't forget to join `debian-malaysia` (OFTC) and `opensource-malaysia` (libera.chat) on matrix, you may join via searching the public room or send this command inside any chat message:
```irc
/join #_oftc_#debian-malaysia:matrix.org #opensource-malaysia:libera.chat
```

See you there everyone!

> What are you waiting for? You're faster than this. Don't think you are, know you are. Come on. Stop trying to hit me and hit me. - Morpheus (The Matrix, 1999)
