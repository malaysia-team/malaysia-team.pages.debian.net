+++ 
draft = false
date = 2021-02-15T17:59:47+08:00
title = "Hello from Debian Malaysia localgroups"
description = ""
slug = "hello-debian-malaysia"
authors = ['Debian Malaysia <debianmalaysia AT SPAMFREE gmail DOT com>']
tags = ['news', 'debian', 'website', 'malaysia', 'hugo', 'linux', 'opensource' ,'localgroups' , 'debian malaysia']
categories = []
externalLink = ""
series = []
+++

## Introduction from Debian Malaysia
Hello everyone! Welcome to "Debian Malaysia" website. Let said something as a few bits of introduction :)

Let talk about "Linux", Linux is the kernel which is part of the operating system. Kernel are supplied with GNU software and other additions software which giving us the name GNU/Linux and here comes the deviation of linux distributions. Debian also known as Debian GNU/Linux, is one of the oldest operating systems based on the linux kernel you can find.

Debian is a beautiful, very versatile  and rock solid linux distro. It is entirely maintained by community with democratic procedures so there are no company politic.  Debian Project, which was established by Ian Murdock on August 16, 1993 and the project are still continue and running because of Debian project are develop by the community around the world.  The project is coordinated over the Internet by a team of volunteers guided by the Debian Project Leader (DPL).

It has a great community and we as "Debian Malaysia" want to be part of it and contribute to Debian Project.

We welcome all Malaysian or foreigner who live in Malaysia to be part of us, move as community and contribute to Malaysia opensource movement.

# Our web stack and purpose of this website
This website is actually just a static weblog generated using Hugo and host on Gitlab pages and mirrored on Debian pages. 

The reason why we choose static website instead of something that need backend engine because it much cleaner, cheaper and easier to handle. The another important reason that driven us to use static website generator is because we can store the source code somewhere inside versioning control repository and it should easier for someone to get involve if s/he want to send new articles, fix bugs or even want to have a whole copy of this website. 

By this method, it can guarantee to let our website available in long term and maintained by community. So come and fork our website repo and send us you pull request.

It no need to spend a single penny on hosting or domain for now. Cheaper solution :)

# Contact
You may reach us via visiting this website for update from time to time or simply subscribe our [RSS feed](../../index.xml) . 

Come and let's get socialize with us via irc/matrix room, telegram group and mailing list. Everyone are welcome!

# Special thanks
We would like to express special our gratitude to anyone who involve and found us before we are publicly shown ourself. Thank you everyone for joining and supporting Debian Malaysia and last not least, thank you official [Debian Localgroups Team](https://wiki.debian.org/Teams/LocalGroups) for warm welcome and support. 

{{< figure src="../../images/Powered_by_Debian.svg.png" caption="Debian Malaysia was here!" >}}