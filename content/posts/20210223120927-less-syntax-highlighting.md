+++ 
draft = false
date = 2021-02-23T12:09:32+08:00
title = "less command and syntax highlighting"
description = ""
slug = "less-syntax-highlighting"
authors = ['Robbi Nespu <robbinespu AT SPAMFREE gmail DOT com>']
tags = ['syntax highlighting', 'debian', 'package', 'less', 'source-highlight', 'bash', 'python-pygments']
categories = []
externalLink = ""
series = []
+++

I fans of `less` package since it available on most operating system (except windows!) and I can open / read large log with (ANSI color) file so fast and you look like hacker on terminal..LOL

But to read the source code, `less -r` don't help much for syntax highlighting. So as alternative I use `source-highlight` package.

```bash
$ sudo apt install less source-highlight
```

then add this lines on your `~/.bashrc` file
```bash
# less RAW (remove ESC chars)
export LESS='-R'
# without line number
alias lessh='LESSOPEN="| /usr/share/source-highlight/src-hilite-lesspipe.sh %s" less -M '
# with line number
alias lesshn='LESSOPEN="| /usr/share/source-highlight/src-hilite-lesspipe.sh %s" less  -nNM '
```

Now reload you bash session with `source ~/.bashrc` and you are done! There is also a package called `python-pygments` which is depend on python dependencies. It has more input language but rather slower.

