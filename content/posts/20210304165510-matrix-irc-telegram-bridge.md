+++ 
draft = false
date = 2021-03-04T16:55:14+08:00
title = "Our Matrix/IRC now bridge with telegram"
description = ""
slug = "matrix-irc-telegram-bridge"
authors = ['Robbi Nespu <robbinespu AT SPAMFREE gmail DOT com>']
tags = ['news','irc','matrix','telegram','debian malaysia','crosspost','chats','xkcd','freenode']
categories = []
externalLink = ""
series = []
+++

{{< figure src="/images/team_chat.png" caption="Opss.. that is you who still love IRC and bridge to other platform" >}}

Since not everyone prefer using telegram and IRC because of various factors, I decide to bridge the communication platform.

```text
|‾‾‾‾‾‾‾‾‾|       |‾‾‾‾‾‾‾‾|       |‾‾‾‾‾‾‾‾‾‾|
|   IRC   | <===> | Matrix | <===> | Telegram |
|_________|       |________|       |__________|
Find us (Debian Malaysia) here!
- IRC      ~ /server freenode /join #debian-malaysia
- Matrix   ~ /join #freenode_#debian-malaysia:matrix.org
- Telegram ~ https://t.me/debianmalaysia
```
Luckly Matrix.org provide a free gateway to Freenode IRC server, which mean I just need to setup a bot on telegram and matrix to bridge each others. This service maybe not permanent since it need dedicate backend running behind to run the bot communication script. 

So, see you there!